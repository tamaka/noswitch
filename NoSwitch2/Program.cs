﻿using System;

namespace CalcPrice {
	[Flags]
	public enum PriceFlag {
		Children,
		Gentlemen,
		Lady,
		Old
	}

	// Price, Programクラスのみ変更
	public abstract class Price {
		public abstract PriceFlag Flag { get; }

		public abstract string GetName(DayOfWeek dayOfWeek);

		public abstract decimal Calculate(DayOfWeek dayOfWeek);

		public static Price Create(PriceFlag flag) => flag switch
		{
			PriceFlag.Children => new ChildrenPrice(),
			PriceFlag.Gentlemen => new GentlemenPrice(),
			PriceFlag.Lady => new LadyPrice(),
			PriceFlag.Old => new OldPrice(),
			_ => throw new Exception("invalid PriceFlag"),
		};
	}

	public interface IPrice {
		PriceFlag Flag { get; }

		decimal Calculate(DayOfWeek dayOfWeek);
		string GetName(DayOfWeek dayOfWeek);
	}

	public class ChildrenPrice : Price, IPrice {
		public override PriceFlag Flag => PriceFlag.Children;

		public override string GetName(DayOfWeek dayOfWeek) => "子供料金";

		public override decimal Calculate(DayOfWeek dayOfWeek) => 500;
	}

	public class GentlemenPrice : Price, IPrice {
		public override PriceFlag Flag => PriceFlag.Gentlemen;

		public override string GetName(DayOfWeek dayOfWeek) => "大人料金";

		public override decimal Calculate(DayOfWeek dayOfWeek) => 2000;
	}

	public class LadyPrice : Price, IPrice {
		private const DayOfWeek LadiesDay = DayOfWeek.Wednesday;

		public override PriceFlag Flag => PriceFlag.Lady;

		public override string GetName(DayOfWeek dayOfWeek) => dayOfWeek == LadiesDay ? "レディースデー料金" : "大人料金";

		public override decimal Calculate(DayOfWeek dayOfWeek) => dayOfWeek == LadiesDay ? 1500 : 2000;
	}

	public class OldPrice : Price, IPrice {
		public override PriceFlag Flag => PriceFlag.Old;

		public override string GetName(DayOfWeek dayOfWeek) => "シニア料金";

		public override decimal Calculate(DayOfWeek dayOfWeek) => 1000;
	}

	public static class Program {
		private static void Main()
		{
			var today = new DateTime(2015, 1, 7);// 水曜日

			// 顧客として、子供、男性、女性、老人が一人ずつ来たとする。
			var customers = new[]{
				Price.Create(PriceFlag.Children),
				Price.Create(PriceFlag.Gentlemen),
				Price.Create(PriceFlag.Lady),
				Price.Create(PriceFlag.Old)
			};

			// 料金表示
			var i = 1;
			foreach (var c in customers) {
				Console.WriteLine(i + ":");
				Console.WriteLine(c.GetName(today.DayOfWeek) + "は" + c.Calculate(today.DayOfWeek) + "円です");
				i++;
			}

			// 合計料金計算
			decimal sum = 0;
			foreach (var c in customers) {
				sum += c.Calculate(today.DayOfWeek);
			}
			Console.WriteLine("合計 : " + sum + "円");
		}
	}
}
