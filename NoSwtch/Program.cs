﻿using System;

namespace NoSwtch {
	internal class Program {
		private static void Main()
		{
			IShape[] shapes = {
				new Triangle { Base = 5, Height = 4 },
				new Circle { Radius = 3 },
				new Rectangle { Width = 4, Height = 4 },
			};

			foreach (var shape in shapes) {
				Console.WriteLine($"AREA: {shape.Area()}");
			}
		}
	}

	internal interface IShape {
		double Area();
	}

	internal class Triangle : IShape {
		public double Base { get; set; }
		public double Height { get; set; }

		public double Area() => Base * Height / 2;
	}

	internal class Circle : IShape {
		public double Radius { get; set; }

		public double Area() => Radius * Radius * Math.PI;
	}

	internal class Rectangle : IShape {
		public double Width { get; set; }
		public double Height { get; set; }

		public double Area() => Width * Height;
	}

	[Flags]
	public enum ErrorLevel {
		None,
		Low,
		High,
		SoylentGreen
	}

	public static class ErrorLevelExtensions {
		public static string ToFriendlyString(this ErrorLevel me) => me switch
		{
			ErrorLevel.None => "Everything is OK",
			ErrorLevel.Low => "SNAFU, if you know what I mean.",
			ErrorLevel.High => "Reaching TARFU levels",
			ErrorLevel.SoylentGreen => "ITS PEOPLE!!!!",
			_ => "Get your damn dirty hands off me you FILTHY APE!",
		};
	}
}
