﻿namespace NoSwitch3 {
	using System;

	internal class PolymorphismTest {
		private static void Main()
		{
			Introduce(new Truepenny("Ky Kiske", 24));       // 正直者のカイさん24歳。
			Introduce(new Liar("Axl Low", 24));             // 嘘つきのアクセルさん24歳。
			Introduce(new Equivocator("Sol Badguy", 24));   // いい加減なソルさん24歳。
			Introduce(new Seventeenist("Ino", 24));         // 時空を超えるイノさん24歳。
		}

		/// <summary>
		/// p さんの自己紹介をする。
		/// </summary>
		private static void Introduce(Person p)
		{
			Console.Write("My name is {0}.\n", p.Name);
			Console.Write("I'm {0} years old.\n\n", p.Age);
		}
	}

	internal class Person {
		protected string name;
		protected int age;

		public Person(string name, int age)
		{
			this.name = name;
			this.age = age;
		}

		public string Name => name;

		public virtual int Age => 0;  // 基底クラスでは特に意味のない値を返す。
	}

	internal interface IAge {
		int Age { get; }
	}

	/// <summary>
	/// 正直者。
	/// 年齢を偽らない。
	/// </summary>
	internal class Truepenny : Person, IAge {
		public Truepenny(string name, int age) : base(name, age) { }

		public override int Age => age; // 実年齢をそのまま返す。
	}

	/// <summary>
	/// 嘘つき。
	/// 鯖を読む(しかも、歳取るにつれ大幅に)。
	/// </summary>
	internal class Liar : Person, IAge {
		public Liar(string name, int age) : base(name, age) { }

		public override int Age => age < 20 ? age : age < 25 ? age - 1 : age < 30 ? age - 2 : age < 35 ? age - 3 : age < 40 ? age - 4 : age - 5; // 年齢を偽る。
	}

	/// <summary>
	/// いいかげん。
	/// 大体の歳しか答えない。
	/// </summary>
	internal class Equivocator : Person, IAge {
		public Equivocator(string name, int age) : base(name, age) { }

		public override int Age => (age + 5) / 10 * 10; // 年齢を四捨五入した値を返す。
	}

	/// <summary>
	/// いくつになったって気持ちは17歳。
	/// </summary>
	internal class Seventeenist : Person, IAge {
		public Seventeenist(string name, int age) : base(name, age) { }

		public override int Age => 17;
	}
}
