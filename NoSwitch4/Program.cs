﻿namespace NoSwitch4 {
	internal class Program {
		private static void Main()
		{
		}

		private JobBase _job = new();

		public string GetJobString() => _job.GetJobString();

		public void NextJob() => _job = _job.ToNext();
	}

	internal class JobBase {
		public virtual string GetJobString() => "無職";

		public virtual JobBase ToNext() => new Wizard();
	}

	internal class Wizard : JobBase {
		public override string GetJobString() => "魔法使い";

		public override JobBase ToNext() => new Priest();
	}

	internal class Priest : JobBase {
		public override string GetJobString() => "僧侶";

		public override JobBase ToNext() => new Sage();
	}

	internal class Sage : JobBase {
		public override string GetJobString() => "賢者";

		public override JobBase ToNext() => new();
	}
}
